//
//  CoreDataManager.swift
//  AITUapp
//
//  Created by Nurba Seyilkhan on 20.06.2021.
//

import Foundation
import CoreData

class CoreDataManager {
    static let shared = CoreDataManager()
    
 lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "NewsCoreData")
        container.loadPersistentStores(completionHandler: {
            (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private init() {}
    
    func allNews() -> [News.Article] {
        let context = persistentContainer.viewContext
        let request: NSFetchRequest<NewsEntity> = NewsEntity.fetchRequest()
        
        let news = try? context.fetch(request)
        
        return news?.map({ News.Article(news: $0 )}) ?? []
    }
    
    func addNews(_ news: News.Article){
        let context = persistentContainer.viewContext
        context.perform {
         
            let addNews = NewsEntity(context: context)
            addNews.title = news.title
            addNews.image = news.urlToImage
         
            
            
        }
        save()
    }

    
    func save(){
        let context = persistentContainer.viewContext
        
        do{
           try context.save()
        }catch {
            print(error)
        }
    }
}
