//
//  NewsEntity.swift
//  AITUapp
//
//  Created by Nurba Seyilkhan on 20.06.2021.
//

import Foundation
import CoreData

class NewsEntity: NSManagedObject{
    
   static func findNews(with id: Int, context: NSManagedObjectContext) -> NewsEntity?{
        let requestResult: NSFetchRequest<NewsEntity> = NewsEntity.fetchRequest()
        requestResult.predicate = NSPredicate(format: "id == %d", id)
        
        do {
            let news = try context.fetch(requestResult)
            if news.count > 0{
                assert(news.count == 1, "Duplicate was found in DB!!")
                return news[0]
            }
        }catch{
            print(error)
        }
        return nil
    }
}
