//
//  DatabaseManager.swift
//  AITUapp
//
//  Created by Nurba Seyilkhan on 13.06.2021.
//

import FirebaseDatabase

public class DatabaseManager{
    static let share = DatabaseManager()
    
    private let database = Database.database().reference()
    
 
    public func canCreateNewUser(with email: String, username:String, completion:(Bool)-> Void){
        completion(true)
    }
    
    public func insertNewUser(with email: String, username:String, completion: @escaping (Bool)-> Void){
        database.child(email.safedatabaseKey()).setValue(["username": username]) { error, _ in
            if error == nil{
                completion(true)
                return
            }else{
                completion(false)
                return
            }
        }
    }
    
}
