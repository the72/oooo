//
//  NewsTableViewCell.swift
//  AITUapp
//
//  Created by Nurba Seyilkhan on 15.06.2021.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageUrl: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var view: UIView!
    static let identifier = String(describing: NewsTableViewCell.self)
    static let nib = UINib(nibName: identifier, bundle: nil)
    
    public var news: News.Article?{
        didSet{
            if let news = news {
                let posterURL = URL(string: (news.urlToImage ?? ""))
                imageUrl.kf.setImage(with: posterURL)
                titleLabel.text = news.title
                
                CoreDataManager.shared.addNews(news)
                

                
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer.cornerRadius = 20
        view.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    
        // Configure the view for the selected state
    }
    
}
